-module({{supid}}).
-behaviour(supervisor).
-export([start_link/0]).
-export([init/1]).

start_link() -> supervisor:start_link({local, ?MODULE}, ?MODULE, []).

% reminder: {Tag, {Mod, Fun, Arg}, RestartType, ShutdownTime, ProcessType, [Mod]}
init([]) ->
    {ok, { {one_for_one, 3, 10},
      [
      ]}}.

