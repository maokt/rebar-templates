-module({{appid}}).
-behaviour(application).
-export([start/2, stop/1]).

start(_StartType, _StartArgs) -> {{supid}}:start_link().

stop(_State) -> ok.
