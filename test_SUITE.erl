-module({{testid}}_SUITE).
-compile(export_all).
-include_lib("common_test/include/ct.hrl").

suite() -> [{timetrap, {seconds, 20}}].

all() -> [ {group, reflection} ].

groups() ->
    [
     { reflection, [parallel,shuffle], find_tests() }
    ].

init_per_suite(C) -> C.
end_per_suite(_C) -> ok.

init_per_group(_G, C) -> C.
end_per_group(_G, C) -> C.

init_per_testcase(_T, C) -> C.
end_per_testcase(_T, C) -> C.

test_{{testid}}(_C) -> {skip, "to do"}.

%<---------------------------------------------------------------------------

find_tests() -> [ Fun || {Fun, 1} <- ?MODULE:module_info(functions), find_tests(Fun) ].
find_tests(A) when is_atom(A) -> find_tests(atom_to_list(A));
find_tests("test_" ++ _) -> true;
find_tests(_) -> false.
