My simple templates for rebar.

    rebar create template=app appid=$MY_APPLICATION_NAME
    rebar create template=srv srvid=$MY_SERVER_NAME
    rebar create template=mod modid=$MY_MODULE_NAME
    rebar create template=sup supid=$MY_SUPERVISOR_NAME
    rebar create template=test testid=$MY_TEST_SUITE_NAME

